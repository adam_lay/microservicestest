﻿using AuthenticationApi.Models;
using Common;
using MongoDB.Driver;

namespace AuthenticationApi.Repositories
{
  public interface IUserRepository
  {
    User GetByEmail(string email);
  }

  public class UserRepository : Repository, IUserRepository
  {
    public User GetByEmail(string email)
    {
      var collection = Database.GetCollection<User>("users");
      var search = collection.Find(u => u.Email == email);
      var user = search.FirstOrDefault();

      return user;
    }
  }
}
