﻿namespace AuthenticationApi.Models
{
  public class LoginTokenRequest
  {
    public string Username { get; set; }
    public string Password { get; set; }
  }
}