﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace AuthenticationApi.Controllers
{
  public abstract class TokenController : ControllerBase
  {
    protected string GetJwtString(string key, Claim[] claims)
    {
      var credentials = new SigningCredentials(
        new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key)),
        SecurityAlgorithms.HmacSha512);

      var token = new JwtSecurityToken(
        issuer: "adam.com",
        claims: claims,
        //expires: DateTime.Now.AddMinutes(30),
        signingCredentials: credentials);

      return new JwtSecurityTokenHandler().WriteToken(token);
    }
  }
}
