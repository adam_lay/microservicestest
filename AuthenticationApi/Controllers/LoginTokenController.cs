﻿using System.Security.Claims;
using AuthenticationApi.Models;
using AuthenticationApi.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace AuthenticationApi.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class LoginTokenController : TokenController
  {
    private readonly IConfiguration _configuration;
    private readonly IUserRepository _userRepository;

    public LoginTokenController(IConfiguration configuration, IUserRepository userRepository)
    {
      _configuration = configuration;
      _userRepository = userRepository;
    }

    // POST api/logintoken
    [HttpPost]
    public IActionResult LoginToken([FromBody] LoginTokenRequest request)
    {
      // Get user from database
      User user = _userRepository.GetByEmail(request.Username);

      // Validate user credentials
      if (user == null || user.Password != request.Password)
        return BadRequest();

      return Ok(new
      {
        token = GetJwtString(_configuration["SecurityKey"], new[]
        {
          new Claim(ClaimTypes.Email, user.Email)
        })
      });
    }
  }
}
