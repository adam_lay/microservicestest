﻿using System.Security.Claims;
using AuthenticationApi.Models;
using AuthenticationApi.Repositories;
using Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace AuthenticationApi.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  [Authorize]
  public class UserTokenController : TokenController
  {
    private readonly IConfiguration _configuration;
    private readonly IUserRepository _userRepository;

    public UserTokenController(IConfiguration configuration, IUserRepository userRepository)
    {
      _configuration = configuration;
      _userRepository = userRepository;
    }
    // POST api/usertoken
    [HttpPost]
    public IActionResult Post([FromBody] UserTokenRequest request)
    {
      string email = User.FindFirstValue(ClaimTypes.Email);

      // Get user from database
      User user = _userRepository.GetByEmail(email);

      // Validate user credentials
      if (user == null)
        return BadRequest();

      return Ok(new
      {
        token = GetJwtString(_configuration["SecurityKey"], new[]
        {
          new Claim(ClaimTypes.Email, user.Email),
          new Claim(ClaimTypes.Name, user.Name),
          new Claim(UpayClaimTypes.UserId, user.Id.ToString())
        })
      });
    }
  }
}
