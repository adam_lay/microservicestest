﻿namespace Common
{
  public static class UpayClaimTypes
  {
    public const string LocalUserId = "LocalUserId";
    public const string UserId = "UserId";
  }
}
