﻿using MongoDB.Driver;

namespace Common
{
  public abstract class Repository
  {
    private readonly MongoClient _client;
    protected readonly IMongoDatabase Database;

    protected Repository()
    {
      _client = new MongoClient("mongodb://localhost:27017");
      Database = _client.GetDatabase("ms");
    }
  }
}
