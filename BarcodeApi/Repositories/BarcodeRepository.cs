﻿using BarcodeApi.Model;
using Common;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BarcodeApi.Repositories
{
  public interface IBarcodeRepository
  {
    Barcode GetForUser(string userId);
  }

  public class BarcodeRepository : Repository, IBarcodeRepository
  {
    public Barcode GetForUser(string userId)
    {
      var collection = Database.GetCollection<Barcode>("barcodes");
      var search = collection.Find(barcode => barcode.UserId == ObjectId.Parse(userId));
      return search.FirstOrDefault();
    }
  }
}
