﻿using System.Security.Claims;
using BarcodeApi.Model;
using BarcodeApi.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BarcodeApi.Controllers
{
  [Route("api/[controller]")]
  [Authorize]
  [ApiController]
  public class BarcodeController : ControllerBase
  {
    private readonly IBarcodeRepository _barcodeRepository;

    public BarcodeController(IBarcodeRepository barcodeRepository)
    {
      _barcodeRepository = barcodeRepository;
    }

    // GET api/barcode
    [HttpGet]
    public IActionResult Get()
    {
      string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

      Barcode model = _barcodeRepository.GetForUser(userId);

      return Ok(model);
    }
  }
}
