﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BarcodeApi.Model
{
  public class Barcode
  {
    public ObjectId Id { get; set; }
    [BsonElement("userId")]
    public ObjectId UserId { get; set; }
    [BsonElement("data")]
    public string Data { get; set; }
  }
}
